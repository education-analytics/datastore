import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

# Use a service account
cred = credentials.Certificate('gsuiteanalytics-firestore-39514537d98a.json')
firebase_admin.initialize_app(cred)

# Creates blank firestore object
db = firestore.client()

# Creates batch object for batch writes
batch = db.batch()

# Reference to events collection
events_ref = db.collection(u'events')

colours = ['big_red', 'big_green', 'big_blue', 'big_purple']

for colour in colours:
    event = events_ref.document(colour)
    batch.set(event, {u'colour' : colour})

#batch.commit()