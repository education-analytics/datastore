import calendar
import time
import datetime
import httplib2

from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from google.oauth2 import service_account

# Global Variables
"""Email of the Service Account"""
SERVICE_ACCOUNT_EMAIL = 'gsa-spc-connector@gsa-spc.iam.gserviceaccount.com'

"""Path to the Service Account's Private Key file"""
SERVICE_ACCOUNT_PKCS12_FILE_PATH = 'gsa-spc-4e703caa7c79.p12'

"""Amount of time to leave in between queries [seconds]"""
INTERVAL = 300

"""Amount of time to keep on fetching queries [seconds]"""
TIMEOUT = 1800

def create_reports_service(user_email):
    """Build and returns an Admin SDK Reports service object authorized with the service accounts
    that act on behalf of the given user.

    Args:
      user_email: The email of the user. Needs permissions to access the Admin APIs.
    Returns:
      Admin SDK reports service object.
    """

    credentials = ServiceAccountCredentials.from_p12_keyfile(
        SERVICE_ACCOUNT_EMAIL,
        SERVICE_ACCOUNT_PKCS12_FILE_PATH,
        'notasecret',
        scopes=['https://www.googleapis.com/auth/admin.reports.audit.readonly'])

    credentials = credentials.create_delegated(user_email)

    http = credentials.authorize(httplib2.Http())

    return build('admin', 'reports_v1', http=http)

service = create_reports_service('googlesync@stpatricks.tas.edu.au')

def event_listener(out):
    """
        1. Input the start date(user)
        2. Store into qlog array.
        3. Pull for period qlog[0] + 1 day
        4. Sort data into array > CSV string
        5. Loop until date = current date
        6. Write file
    """
    
    # time = "2018-09-20T08:00:00.000Z"
    start_time = datetime.datetime(2018, 9, 20)
    arr = []

    
    for x in range(6):
        end_time = start_time + datetime.timedelta(hours=(24))
        arr.append(end_time)

        # Formats the start time and prepares for maximum Jewgle wank
        str_start_time = str((start_time).isoformat('T')[:-3]+':00.000Z')
        str_end_time = str((end_time).isoformat('T')[:-3]+':00.000Z')

        # Sends Query to API
        query_result = service.activities().list(
            userKey='all', 
            applicationName='drive', 
            startTime=str_start_time, 
            endTime=str_end_time
        ).execute()

        activities = query_result.get('items', [])

        if not activities:
            print('No logins found.')
        else:
            for activity in activities:
                try:
                    doc_id = activity['events'][0]['parameters'][1]['name']
                except KeyError:
                    doc_id = -1

                try:
                    doc_type = activity['events'][0]['parameters'][2]['value']
                except KeyError:
                    doc_type = -1
                
                try:
                    doc_title = activity['events'][0]['parameters'][3]['value']
                except KeyError:
                    doc_title = -1

                event = activity['events'][0]['type']
                name = activity['events'][0]['name']
                actor = activity['actor']['email']
                time = acitivity['id']['time']

                x = ('\n{0},{1},{2},{3},{4},{5}'.format(time, event, name, actor, doc_id, doc_type, doc_title))
                out = out + x

    return out
    
# Opens the text file for appending
txt = open('out.csv', "a")

# Init out
out = str("event, name, actor, doc_id, doc_title")  

# Runs the function and writes the file
c = event_listener(out)
print(c)
txt.write(c)
