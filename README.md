Datastore
==========
This project holds all of the code used to pull data from various APIs 
(at this moment, purely events from Google's Reports API).

Additionally holds the code that stores said data into relevant Firestore instances.

Since a lot of this code is not intended to explicitly work together, and this is
essentially a nice working directory, master branch is not explictly for release
code, just code that is considered finished at the time.

Contributing
============
 - <a href="https://sethrobertson.github.io/GitBestPractices/">Follow this general guide for conventions</a>

Naming Conventions:
-------------------

 - Commit messages should follow the format defined <a href="https://udacity.github.io/git-styleguide/">here</a>.<br />
 - When branching, follow the format defined in <a href="https://stackoverflow.com/questions/273695/git-branch-naming-best-practices">this Stack Overflow post</a>.<br />
 - The header of each file should hold information about the intended filename, contributers
   and a brief description, in the following format.<br />

   ```python
   """
       File: $filename.extension
       Authors: $author1, $author2

       Description:
       $brief description
   """
   ```


Merge Request Process:
---------------------

 - Submit request. <br />
 - Wait for approval from any other party before approving any merge requests to release branch *[master]*. <br />
 - Merge to *master* on approval from other party. <br />
